



class SmallItemData{
  final String imagePath;
  final String name;
  final String city;
  final String rating;
  final String info;

  const SmallItemData({ required this.name,required this.imagePath, required this.city ,required this.rating ,required this.info});
}