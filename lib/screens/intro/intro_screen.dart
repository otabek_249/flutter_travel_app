import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../home/home_screen.dart';

class IntroScreen extends StatelessWidget {
  const IntroScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: const Color(0xff031F2B),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32),
        child: Column(
          children: [
            const Spacer(),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 32),
              height: 195,
              child: Image.asset(
                "assets/images/romantic.png",
              ),
            ),
            const SizedBox(
              height: 28,
            ),
            Text(
              "Oddiy hayotdan qoching",
              style: GoogleFonts.poppins(
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: screenWidth>750 ? 22 : 21,
              ),
            ),
            Text(
              "Atrofingizdagi ajoyib tajribalarni kashf eting va sizni qiziqarli yashashga majbur qiling!",
              textAlign: TextAlign.center,
              style: GoogleFonts.poppins(
                fontSize: screenHeight>=750 ? 14 : 12,
                fontWeight: FontWeight.w300,
                color: const Color(0xffd6d2d2),
              ),
            ),

            const SizedBox(height: 40),
            const Spacer(),
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => const HomeScreen(),
                  ),
                );
              },
              child: Container(
                margin: const EdgeInsets.only(
                  bottom: 66,
                ),
                height: 52,
                width: double.infinity,
                decoration: const BoxDecoration(
                  color: Color(0xff5EDFFF),
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
                child: Center(
                  child: Text(
                    "Boshladik",
                    style: GoogleFonts.poppins(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: const Color(0xff263238),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
