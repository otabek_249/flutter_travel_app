
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:travel_app/data/small_item_data.dart';

class InfoScreen extends StatefulWidget {
  final SmallItemData data;
  const InfoScreen({super.key, required this.data});

  @override
  State<InfoScreen> createState() => _InfoScreenState();
}

class _InfoScreenState extends State<InfoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff031F2B),
      body: CustomScrollView(
        slivers: [
          SliverAppBar(

            pinned: false,
            floating: true,
            backgroundColor: const Color(0xff031F2B),
            automaticallyImplyLeading: false,
            expandedHeight: 300,
            flexibleSpace: FlexibleSpaceBar(
                background: Container(
              height: 200,
              width: double.infinity,
              decoration:  BoxDecoration(
                  borderRadius: const BorderRadius.only(bottomRight: Radius.circular(30),bottomLeft: Radius.circular(30)),
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(widget.data.imagePath))),
              child: Column(
                children: [
                  const Spacer(),
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.data.name,
                              style: GoogleFonts.poppins(
                                  color: Colors.white, fontSize: 20),
                            ),
                            Text(
                              widget.data.city,
                              style: GoogleFonts.poppins(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.white),
                            )
                          ],
                        ),
                      ),
                      const Spacer(),
                      Padding(
                        padding: const EdgeInsets.only(right: 29),
                        child: Row(
                          children: [
                            Text(
                              widget.data.rating,
                              style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500),
                            ),
                            const SizedBox(
                              width: 2,
                            ),
                            const Icon(
                              Icons.star,
                              color: Color(0xFFE58F3F),
                              size: 24,
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  )
                ],
              ),
            )),
          ),
          SliverToBoxAdapter(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 31,top: 16,bottom: 16),
                  child: Text(
                    "Batafsil",
                    style: GoogleFonts.poppins(
                      fontSize: 18,
                      color:Colors.white,
                      fontWeight: FontWeight.w500
                    ),
                  )
                ),
                Padding(
                    padding: const EdgeInsets.only(left: 31,bottom: 16,right: 31),
                    child: Text(
                      textAlign: TextAlign.justify,
                      widget.data.info,
                      style: GoogleFonts.poppins(
                          fontSize: 14,
                          color:Colors.white,
                          fontWeight: FontWeight.w400
                      ),
                    )
                ),



              ],
            ),
          ),


        ],
      ),
    );
  }
}
