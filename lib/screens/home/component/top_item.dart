import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TopItem extends StatefulWidget {
  final String name;
  final bool isCheck;
  final VoidCallback onTap;

  const TopItem({super.key, required this.name, required this.isCheck,required this.onTap});

  @override
  State<TopItem> createState() => _TopItemState();
}

class _TopItemState extends State<TopItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal:8),
        child: Container(
          width: widget.name.length > 4 ? 80 : 70,
          height: 34,
          decoration: BoxDecoration(
              color: widget.isCheck
                  ? const Color(0xff5edfff)
                  : const Color(0xff263238),
              borderRadius: BorderRadius.circular(8)),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
            child: Center(
              child: Text(
                widget.name,
                style: GoogleFonts.poppins(
                    color: widget.isCheck ? const Color(0xff263238) : Colors.white,
                    fontSize: 12,
                    fontWeight: FontWeight.w400),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
