
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:travel_app/data/small_item_data.dart';
class SmallImageItem extends StatefulWidget {
  final SmallItemData data;
  final VoidCallback onTap;
  const SmallImageItem({super.key  ,required this.data  ,required this.onTap});

  @override
  State<SmallImageItem> createState() => _SmallImageItemState();
}

class _SmallImageItemState extends State<SmallImageItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        margin: const EdgeInsets.only(right: 18),
        height: 178,
        width: 124,
        decoration:  BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(8)),
          image: DecorationImage(
            fit: BoxFit.fill,
            image: NetworkImage(widget.data.imagePath),
          )
        ),
        child: Align(
          alignment: Alignment.bottomLeft,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Spacer(),
                Text(widget.data.name,style: GoogleFonts.poppins(
                  fontSize: 12,
                  color: Colors.white,
                  fontWeight: FontWeight.w500
                ),),
                Text(widget.data.city,style: GoogleFonts.poppins(
                    fontSize: 10,
                    color: Colors.white,
                    fontWeight: FontWeight.w300
                ),),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
