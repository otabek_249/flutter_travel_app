
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:travel_app/data/small_item_data.dart';
class BigImageItem extends StatefulWidget {
  final VoidCallback onTap;
  final SmallItemData data;
  const BigImageItem({super.key, required this.onTap,required this.data});

  @override
  State<BigImageItem> createState() => _BigImageItemState();
}

class _BigImageItemState extends State<BigImageItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 10.0),
        child: Container(
          height: 130,
          width: double.infinity,
          decoration:  BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(8)),
            image: DecorationImage(
              fit: BoxFit.cover,
              image: NetworkImage(widget.data.imagePath)
            )
          ),
          child: Column(
            children: [
              const Spacer(),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(widget.data.name, style: GoogleFonts.poppins(
                          color: Colors.white,
                          fontSize: 12
                        ),),
                        Text(widget.data.city,
                        style: GoogleFonts.poppins(
                          fontSize: 10,
                          fontWeight: FontWeight.w300,
                          color: Colors.white
                        ),)
                      ],
                    ),
                  ),
                  const Spacer(),
                   Padding(
                     padding: const EdgeInsets.only(right: 29),
                     child: Row(
                      children: [
                        Text(widget.data.rating,
                        style: GoogleFonts.poppins(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w500
                        ),),
                        const SizedBox(width: 2,),
                        const Icon(Icons.star,color: Color(0xFFE58F3F),size: 19,)
                      ],
                                     ),
                   )


                ],
              ),
              const SizedBox(height: 10,)
            ],
          ),
        ),
      ),
    );
  }
}
