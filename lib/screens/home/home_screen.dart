import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:travel_app/data/small_item_data.dart';
import 'package:travel_app/screens/home/component/big_image_item.dart';
import 'package:travel_app/screens/home/component/small_image_item.dart';
import 'package:travel_app/screens/home/component/top_item.dart';
import 'package:travel_app/screens/info/info_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});


  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  var index =0;
  var searchText ="";
  var isClick1 =true;
  var isClick2 = false;
  var isClick3 = false;
  var isClick4 = false;
  var list =getListFromMap(0);



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color(0xff031f2b),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color(0xff031f2b),
        title: Padding(
          padding: const EdgeInsets.only(left: 14),
          child: Text(
            "Sayohat",
            style: GoogleFonts.poppins(
                color: Colors.white, fontSize: 24, fontWeight: FontWeight.w600),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
               TextField(
                onChanged: (name){
                  setState(() {

                  });
                  searchText = name;
                },
                style: const TextStyle(color: Colors.grey),
                decoration: const InputDecoration(
                  hintText: "Search",
                  hintStyle: TextStyle(color: Colors.grey),
                  filled: true,
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.grey,
                  ),
                  fillColor: Color(0xff263238),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Color(0xff263238)),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    borderSide: BorderSide(color: Color(0xff263238)),
                  ),
                ),
              ),
              const SizedBox(height: 20),
              SizedBox(
                height: 34,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    TopItem(isCheck: isClick1, name: "Barchasi", onTap: () {
                      setState(() {
                        list = getListFromMap(0);
                        isClick1 = true;
                        isClick2 = false;
                        isClick3 = false;
                        isClick4 = false;
                      });

                    },),
                    TopItem(isCheck: isClick2, name: "Tog'", onTap: () {
                      setState(() {
                        list = getListFromMap(1);
                        isClick1 = false;
                        isClick2 = true;
                        isClick3 = false;
                        isClick4 = false;
                      });
                    },),
                    TopItem(isCheck: isClick3, name: "Dacha", onTap: () {
                      setState(() {
                        list = getListFromMap(2);
                        isClick1 = false;
                        isClick2 = false;
                        isClick3 = true;
                        isClick4 = false;
                      });
                    },),
                    TopItem(isCheck: isClick4, name: "Shahar", onTap: () {
                      setState(() {
                        list = getListFromMap(3);
                        isClick1 = false;
                        isClick2 = false;
                        isClick3 = false;
                        isClick4 = true;
                      });
                    },),
                  ],
                ),
              ),
              const SizedBox(height: 23),
              Text(
                "Mashxur joylar",
                style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
                    color: Colors.white),
              ),
              const SizedBox(height: 16),
              SizedBox(
                height: 178,
                child: ListView.builder(

                  scrollDirection: Axis.horizontal,
                  itemCount: searchText.isNotEmpty?searchByName(searchText).length:list.length,
                  itemBuilder: (context, index) {
                    return SmallImageItem(
                      data: searchText.isNotEmpty?searchByName(searchText)[index]:list[index],
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) =>
                                InfoScreen(data: searchText.isNotEmpty?searchByName(searchText)[index]:list[index]),
                          ),
                        );
                      },
                    );
                  },
                ),
              ),
              const SizedBox(height: 43),
              Text(
                "Barchasi",
                style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                    fontSize: 16),
              ),
              const SizedBox(height: 16),
              ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount:  searchText.isNotEmpty?searchByName(searchText).length:list.length,
                itemBuilder: (context, index) {
                  var data =  searchText.isNotEmpty?searchByName(searchText):list;
                  return BigImageItem(
                    data: data[index],
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => InfoScreen(data: data[index]),
                        ),
                      );
                    },
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}


final cityData = <SmallItemData>[
  const SmallItemData(
      name: "New York City",
      imagePath: "https://cdn.britannica.com/61/93061-050-99147DCE/Statue-of-Liberty-Island-New-York-Bay.jpg",
      city: "New York",
      rating: "4.9",
      info: "New York City is a global hub of finance, art, media, and culture, known for its iconic skyscrapers, vibrant neighborhoods, and diverse population."
  ),
  const SmallItemData(
      name: "Tokyo",
      imagePath: "https://content.r9cdn.net/rimg/dimg/ca/7e/9ae1c4b2-city-21033-16374d0645f.jpg?crop=true&width=1020&height=498",
      city: "Tokyo",
      rating: "4.8",
      info: "Tokyo is the bustling capital of Japan, renowned for its high-tech innovation, unique blend of traditional and modern culture, and dynamic cityscape."
  ),
  const SmallItemData(
      name: "Paris",
      imagePath: "https://res.klook.com/image/upload/cities/a2e9rovyxdxakpru1utw.jpg",
      city: "Paris",
      rating: "4.7",
      info: "Paris, the City of Light, is renowned for its rich history, iconic landmarks, world-class cuisine, and charming Parisian atmosphere."
  ),
  const SmallItemData(
      name: "London",
      imagePath: "https://assets.vogue.in/photos/5ce43d58f55c27a7f4a28dce/2:3/w_1920,c_limit/London-City-Travel-Guide-Vogue-India.jpg",
      city: "London",
      rating: "4.6",
      info: "London is a global center of finance, arts, and culture, with a diverse population, historical landmarks, and a vibrant, cosmopolitan atmosphere."
  ),
  const SmallItemData(
    name: "Sydney",
    imagePath: "https://media.tatler.com/photos/6141d37b9ce9874a3e40107d/16:9/w_1920,c_limit/social_crop_sydney_opera_house_gettyimages-869714270.jpg",
    city: "Sydney",
    rating: "4.8",
    info: "Sydney is a stunning coastal city in Australia, known for its iconic Opera House, picturesque harbor, and lively, multicultural vibe."
  )
    ];

final List<SmallItemData> travelData = [
  const SmallItemData(
    name: "Orol Dengiz",
    imagePath: "https://encrypted-tbn2.gstatic.com/licensed-image?q=tbn:ANd9GcRiOFb3L-6kntuTldQddVK9sQxo1TdAHp5Zr_5J7JToJZHIiW-_OMK_TTXi-FB83DmhiWb1BpwzDrdm-0NFI_2oUY2n7qnNkLseEmf4ng",
    city: "Qoraqalpog'iston",
    rating: "4.5",
    info: "Orol dengizi – Oʻrta Osiyodagi eng katta berk koʻl. Maʼmuriy jihatdan Orol dengizining yarmidan koʻproq janubi-gʻarbiy qismi Oʻzbekiston, shimoli-sharqiy qismi Qozogʻiston hududida joylashgan. Oʻtgan asrning 60-yillarigacha Orol dengizi maydoni orollari bilan oʻrtacha 68,0 ming km² ni tashkil etgan. Kattaligi jihatidan dunyoda toʻrtinchi (Kaspiy dengizi, Amerikadagi Yuqori koʻl va Afrikadagi Viktoriya koʻlidan keyin), Yevrosiyo materigida (Kaspiydan keyin) ikkinchi oʻrinda edi. Dengiz shimoli-sharqdan janubi-gʻarbga choʻzilgan, uz. 428 km, eng keng joyi 235 km (45° shahrik.) boʻlgan. Havzasining maydoni 690 ming km², suvining hajmi 1000 km³, oʻrtacha chuq. 16,5 m atrofida oʻzgarib turgan. Havzasining kattaligi uchun dengiz deb atalgan. Orol dengizi yuqori pliotsenda Yer poʻstining egilgan yeridagi botiqda hosil boʻlgan. Tubining relyefi (gʻarbiy qismini hisobga olmaganda) tekis. Orol dengizida juda koʻp yarim orol va qoʻltiqlar boʻlgan. Shimol qirgʻoklarida eng katta qoʻltiqlaridan Chernishev, Paskevich, Sarichigʻanoq, Perovskiy, janubi-sharqiy va sharqiy qirgʻoqlarida Tushbas, Ashshibas, Oqsagʻa, Suluv va boshqa, Amudaryo bilan Sirdaryo quyiladigan joylarida Ajiboy, Tolliq, Jiltirbas qoʻltiklari, Qulonli va Moʻynoq yirik yarim orollari boʻlgan. Orol dengizida qadimdan suv sathi goh koʻtarilib, goh pasayib turgani maʼlum. Keyingi geologik davrda Sariqamish va Oʻzboʻy orqali Orol dengizi suvi vaqt-vaqti bilan Kaspiyga quyilgan, suv sathi ancha baland boʻlib, jan.vajan.-sharqidagi bir necha ming km² maydonli sohil suv ostida boʻlgan. Orol dengizi unchalik chuqur emas. Chuqur joylari Orol dengizining gʻarbiy qismida. Qoraqalpogʻistonning shimoliy qismi Ustyurt tegisligi yonida chuqurligi 69 m gacha yetgan.",
  ),
  const SmallItemData(
    name: "Registon Meydoni",
    imagePath: "https://www.trtavaz.com.tr/uploads/photos/2021/05/04/7b568ce744a6426bb0843e2c00b6963b.jpg?w=640",
    city: "Samarqand",
    rating: "4.8",
    info: "Dastlabki 1417–1420-yillarda Ulugʻbek madrasasi bunyod etilib, keyinchalik qarshisiga – maydonning sharqiy qismida Ulugʻbek xonaqosi (1424-yil), shimoliy qismiga Mirzoyi karvonsaroyi, janubiga Alika Koʻkaldosh juma masjidi (1430-yil) bunyod etilgan, yonida esa yogʻochdan xotamkori uslubida Masjidi Muqatta va Abusaid madrasasi qurilgan. 1420–1440-yillarida Registon hashamatli meʼmoriy ansamblga aylangan. XVII asrda Samarqand hokimi Yalangtoʻsh Bahodir vayrona holatdagi Ulugʻbek xonaqosi oʻrniga Sherdor madrasasini (1619–1635/36), Mirzoyi karvonsaroyi oʻrniga Tillakori masjidini (1646/47–1659/60) qurdirgan. Registon maydoni oʻzining rang-barang koshinkori bezaklari; naqshinkori peshtoqlari, ulkan gumbazlari bilan Markaziy Osiyo meʼmorchligining noyob yodgorligi hisoblanadi[2].Qadimiy Samarqand shahrining rasmiy markazi Registon maydoni boʻlib, bu yerda uchta madrasa qad koʻtargan: Ulugʻbek, Sherdor va Tillakori madrasalari. Registon – qadimiy ilm, taʼlim muassasalari joylashgan joy boʻlib, sharqdagi shahar qurilishi sanʼatining eng koʻzga koʻrinarli namunalaridan biri hisoblanadi. U haqda Temuriylar faxr bilan: „Kim bizning kuch-qudratimizga shubha qilsa, kelib biz qurgan binolarni koʻrsin“, deganlar. 2001-yilda bu uch madrasa UNESCOning butun dunyo yodgorliklari roʻyxatiga kiritilgan.",
  ),
  const SmallItemData(
    name: "Xiva Ichan Qal'a",
    imagePath: "https://surdik.uz/contentImage/1627151107533.jpg",
    city: "Xiva",
    rating: "4.7",
    info: "Xorazm xalq meʼmorligining ajoyib obidalari: madrasa, masjid, saroy va minoralar, asosan, Ichan qalʼada joylashgan. Undagi meʼmoriy yodgorliklarning yaratilish tarixi, asosan, 4 davrga taalluqli: birinchisi Xorazmning qadimgi davridan to moʻgʻullar istilosi davriga qadar, bu davrdan Koʻhna arkning gʻarbiy devori, qalʼa devorining shimoli-sharqiy burchagidagi qadimgi davrga mansub burj va qalʼa devori qoldiqlari saqlanib qolgan. Ikkinchisi Xorazmning 1220-yildagi moʻgʻullar istilosidan keyingi tiklanish davri. Bu davrda Sayid Alovuddin maqbarasi va boshqalar mahobatli binolar qurilgan. Uchinchi davri XVI—XVII asrlarga toʻgʻri keladi. Shu paytda (Abulgʻozixon va Asfandiyorxon hukmronlik davrida) Ichan qalʼada Anushaxon hammomi (1657), peshayvonli Oq masjid (1675), Xoʻjamberdibiy madrasasi (1688) kabilar bunyod etildi. Koʻhna ark istehkomlari mustaqkamlandi, koʻrinishxona (xonning qabulxonasi) qurildi (1686—1688). Buxoro bilan Eron oʻrtasida Xiva xonligi uchun boshlangan urush natijasida (XVIII asrning 1-yarmi) Ichan qalʼa va, umuman, Xiva shahri qattiq shikastlandi (Xiva bir qancha vaqt Eronga tobe viloyat boʻlib turdi).",
  ),
  const SmallItemData(
    name: "Chimyon Tog'lari",
    imagePath: "https://uzbekistan.travel/storage/app/media/nargiza/cropped-images/beldersay-0-0-0-0-1590648711.jpg",
    city: "Toshkent",
    rating: "4.6",
    info: "Katta Chimyon toʻgi – Chotqol tizmasining bir qismi boʻlgan juda katta gumbazsimon togʻ tizmasi. Oʻzbekistonning Toshkent viloyati Boʻstonliq tumanida, Toshkentdan 80 km sharqda joylashgan. Toshkent viloyatidagi Chatqol tizmasining tarkibiga kiruvchi ulkan gumbazsimon massiv. Eng baland joyi – Katta Chimyon choʻqqisi boʻlib, dengiz sathidan 3309 m balandlikda joylashgan. Chimyonsoy vodiysini janubidan oʻrab turadi. Paleozoy orogenetik jarayonida burmalanib granit, granodiorit, diorit, granit-porfir, kvarsli porfir, siyenit, siyenitli diorit, gabbro kabi intruziv jinslar va ohaktosh hamda qumtoshlardan tuzilgan. Relyefi kuchli darajada parchalangan. Yon bagʻirlar oʻrtacha 25—35°, baʼzi joylarida 45° va undan ortiq qiyaliglari mavjud. Janubi-gʻarbdan shimoli-sharq tomon choʻzilgan",
  ),
  const SmallItemData(
    name: "Buxoro Ark",
    imagePath: "https://adrastravel.com/wp-content/uploads/2021/04/bukhara-ark.webp",
    city: "Buxoro",
    rating: "4.9",
    info: "Buxoro arki — Oʻzbekistonning Buxoro shahridagi qadimiy ark; atrofidagi maydon sathidan qariyb 20 metr balandlikda koʻtarilgan va taxminan 4 gektar maydonni egallagan monumental qal’a. Qalʼa Buxoroning eng qadimiy meʼmoriy va arxeologik yodgorligi hisoblanadi. Bu qal’a tepalikni tashkil etgan koʻp asrlik vayronagarchilik qatlamlariga ega shaharning eng qadimiy qismi hisoblanadi.O‘z vaqtida Registon maydoni ustida qad ko‘targan Ark buyuklik, qudrat va o‘tib bo‘lmaslik timsoli bo‘lgan. Ark devorlaridan birida bir vaqtlar Buxoro amirligidagi qudrat ramzi bo‘lgan katta charm qamchi osilgan Buxoro arki va uning oldidagi bozor. 1890-yilla Arkning orqa yon tomonidan koʻrinishi Arkning orqa yon tomonidan koʻrinishi Buxoro arkining yon tomonidan koʻrinishi Buxoro arkining yon tomonidan koʻrinishi Arkning 1902-yildagi koʻrinishi Arkning 1902-yildagi koʻrinishi Buxoro arkining 1907-yildagi koʻrinishi Buxoro arkining 1907-yildagi koʻrinishi",
  ),
  const SmallItemData(
    name: "Farg'ona Vodiy",
    imagePath: "https://storage.kun.uz/source/uploads/20190104/1871871874055.jpg",
    city: "Farg'ona",
    rating: "4.4",
    info: "Fargʻona vodiysi (Fargʻona soyligi) — Oʻrta Osiyodagi togʻlar orasida joylashgan vodiy, Oʻrta Osiyoning yirik togʻ oraligʻi (soylik) botiqlaridan biri. Shimoldan Tyan-Shan, janubdan Hisor-Olay togʻ tizmalari bilan oʻralgan. Asosan, Oʻzbekiston, qisman Qirgʻiziston va Tojikiston Respublikalari hududida joylashgan. Katta qismi Turkiston va Olay tizmalarining shimoliy yonbagʻirlariga borib taqaladigan uchburchak shaklida boʻlib, shimoli-gʻarbdan Qurama tizmasi va Chatqol tizmasi, shimoli-sharqdan Fargʻona tizmasi bilan oʻralgan. Gʻarbda (eni 8-10 km) „Xoʻjand darvozasi“ orqali Toshkent — Mirzachoʻl botigʻi bilan tutashgan. Uzunligi 300 km, eni 60-120 km, eng keng joyi 170 km, maydoni 22 ming km². Balandligi gʻarbida 330 m, sharqdda 1000 m. Fargʻona vodiysining umumiy tuzilishi ellipssimon (bodomsimon) shaklda boʻlib, gʻarbdan sharqqa tomon kengayib boradi. Fargʻona vodiysi yer yuzasi toʻrtlamchi sistemaning allyuviy va prolyuviy choʻkindi togʻ jinslari bilan toʻlgan. Soylik toshkoʻmir davrida egik shaklda boʻlib, oʻrta toshkoʻmir davrida qalin qumtosh — loyli choʻkindilar bilan qoplangan boʻlgan. Boʻr davrida esa sayoz dengiz boʻlgan. Paleogen davrining oxiriga kelib, batamom quruklikka aylangan. Soylik atrofidagi togʻ tizmalari Alp burmalanishida keskin koʻtarila boshlagan, lekin denudatsiya jarayonida qaytadan yemirilib ketgan. Vodiy tubidagi dengiz yotqiziqlari ustini qalinligi 300-400 metrli kontinental yotqiziqlar qoplagan",
  ),
  const SmallItemData(
    name: "Amir Temur Maydoni",
    imagePath: "https://as2.ftcdn.net/v2/jpg/01/35/85/33/1000_F_135853371_045C00gc0V58L19FTSo64kh93Fw2m8Yd.jpg",
    city: "Toshkent",
    rating: "4.7",
    info: "Amir Temur xiyoboni – Oʻzbekiston Respublikasi poytaxti Toshkent shahri markazidagi xiyobon. Xiyobon 1882-yilda Turkiston general-gubernatori Mixail Chernyayev tashabbusi va arxitektor Nikolay Ulyanov loyihasi asosida barpo etilgan. Xiyobonda temuriylar davlati sarkardasi Amir Temur haykali oʻrnatilgan, haykal atrofi boʻylab yoʻlaklar yotqizilgan va daraxtlar ekilgan. Xiyobon hozirgi koʻrinishga 2009-yilda amalga oshirilgan ishlardan soʻng kelgan. 1993-yilgacha xiyobon Inqilob xiyoboni, undan oldin esa Konstantin xiyoboni yoki Kaufman xiyoboni deb nomlangan.",
  ),
  const SmallItemData(
    name: "Shahrisabz",
    imagePath: "https://people-travels.com/storage/images/QQ4jWThIWjoBaR1gvCggDL47E68KITG2Fr6WxGuW.webp",
    city: "Shahrisabz",
    rating: "4.8",
    info: "Shahrisabz — Oʻzbekiston Respublikasining Qashqadaryo viloyatidagi shahar. Shahrisabz tumani maʼmuriy markazi (1926-yildan). Kitob—Shahrisabz vohasida. Katta Oʻzbekiston trakti yoqasida joylashgan. Shahrisabz jan.dan Tanxozdaryo, shim.dan Oqsuv daryosi oqib oʻtadi. Urtacha bal. 658 m. Aholisi 174,8 ming kishi (2021 https://data.gov.uz/uz/datasets/908?dp-1-sort=G6[sayt ishlamaydi]). Sh. Oʻzbekistondagi qadimiy shaharlardan biri. Arxeologik maʼlumotlarga koʻra, shaharga mil. av. 1 ming yillikning oʻrtalarida asos solingan.Shahrisabz mil. boshlarida va ilk oʻrta asrlarda turli nomlar bilan atalgan (qarang Kesh). 13-asrdan Shahrisabz nomi ham urf boʻlgan.",

  ),
  const SmallItemData(
    name: "Toshkent Tower",
    imagePath: "https://i.pinimg.com/564x/f9/dc/32/f9dc3248b3e3f4e002961882842c569f.jpg",
    city: "Toshkent",
    rating: "4.5",
    info: "XX asrning 60-yillariga kelib televideniye va radio O‘zbekiston madaniy hayotida mustahkam joy egallay boshladi. 1957-yilda bunyod etilgan 180 metrlik birinchi Oʻzbekiston telemarkazi 4 millionlik poytaxt va Toshkent viloyati aholisi ehtiyojini yetarlicha qondirish, televizion koʻrsatuvlarni koʻpaytirish va togʻli tumanlarga dasturlarni yetkazishga ojizlik qilib qoldi. 1971-yilning 1-sentyabrida Oʻzbekiston Aloqa vazirligi (hozirgi Respublika Aloqa va axborotlashtirish agentligi) tasarrufida Toshkent radiotelevizion uzatish markazi qurilishi direksiyasi tashkil etildi. Qisqa fursat ichida yangi teleminora loyihasini tayyorlashga kirishildi. Loyiha juda sinchkovlik bilan oʻrganildi, qayta-qayta sinovlardan oʻtkazildi. Chunki bunday balandlikdagi inshoot qurilishi hali tajribada boʻlmagandi. 1978-yilda teleminora qurilishi boshlandi. Har qanday inshootning mustahkamligi albatta poydevoriga bogʻliq. Teleminoraning toʻrt tayanchi va unga tutashgan uchta oyoqlari 11 metrlik chuqurlikdan koʻtarilib chiqilgan, bir-biri bilan bogʻlanmagan temir-beton asoslarga tayangan boʻlib, muvozanatini oʻta yuqori darajada ushlab turishga qodir klassik tizim yaratildi.",
  ),
  const SmallItemData(
    name: "Muynak",
    imagePath: "https://www.advantour.com/img/uzbekistan/images/moynak.jpg",
    city: "Qoraqalpog'iston",
    rating: "4.3",
    info: "Moʻynoq – Qoraqalpogʻiston Respublikasi Moʻynoq tumanidagi shahar (1963-yil 27 iyuldan). Tuman markazi. Qoraqalpogʻistonning eng shim. shahri. Orol dengizining suvi chekinmasdan avval dengizning janubiy-gʻarbiy soqilida joylashgan edi. Nukus shahridan 180 km shimolida, yaqin temir yoʻl stansiyasi – Qoʻngʻirot (100 km). Yanvarning oʻrtacha temperaturasi —7,4°, iyulniki 26,3°. Kuchli shamollar boʻlib turadi. Aholisi 13,6 ming kishi (2000). Oʻtmishda shahar sanoatida hunarmandchilik asosiy oʻrin tutgan. Konserva kombinati, paxta tozalash, non zavodlari, bosmaxona, madaniy va maishiy xizmat shoxobchalari, umumiy taʼlim maktablari, sanoat transport kasbhunar kolleji, biznes maktabi, kutubxona, klub, madaniyat va istirohat bogʻi, stadion, choʻmilish xavzasi, kasalxona bor. Orol dengizining Moʻynoqdan uzoklashishi, murakkab ekologik holat shahar qurilishiga, sanoatning rivoji va madaniy hayotiga oʻz taʼsirini oʻtkazdi",
  ),
];


final List<SmallItemData> mountainList = [
  const SmallItemData(
      name: "Mount Everest",
      imagePath: "https://cdn.mos.cms.futurecdn.net/D9bzCVeZLHQnZ6bUWvAkrW-650-80.jpg.webp",
      city: "Solukhumbu, Nepal",
      rating: "5.0",
      info: "Mount Everest is the highest mountain in the world, standing at 29,032 feet (8,849 meters) above sea level. It is located in the Mahalangur Himal sub-range of the Himalayas."
  ),
  const SmallItemData(
      name: "K2",
      imagePath: "https://upload.wikimedia.org/wikipedia/commons/c/c9/Chogori.jpg",
      city: "Gilgit-Baltistan, Pakistan",
      rating: "4.9",
      info: "K2, also known as the Savage Mountain, is the second-highest mountain in the world at 28,251 feet (8,611 meters) above sea level. It is located in the Karakoram range on the border of Pakistan and China."
  ),
  const SmallItemData(
      name: "Kangchenjunga",
      imagePath: "https://shorturl.at/DmLFs",
      city: "Sikkim, India",
      rating: "4.8",
      info: "Kangchenjunga is the third-highest mountain in the world at 28,169 feet (8,586 meters) above sea level. It is located in the Himalayas on the border of Nepal and India."
  ),
  const SmallItemData(
      name: "Lhotse",
      imagePath: "https://shorturl.at/6qJqU",
      city: "Khumbu, Nepal",
      rating: "4.7",
      info: "Lhotse is the fourth-highest mountain in the world at 27,940 feet (8,516 meters) above sea level. It is part of the Everest massif and is located in the Mahalangur Himal sub-range of the Himalayas."
  ),
  const SmallItemData(
      name: "Makalu",
      imagePath: "https://www.dreamersdestination.com/images/packages/mt-makalu.jpg",
      city: "Sankhuwasabha, Nepal",
      rating: "4.6",
      info: "Makalu is the fifth-highest mountain in the world at 27,766 feet (8,485 meters) above sea level. It is located in the Mahalangur Himal sub-range of the Himalayas, about 14 miles (23 km) southeast of Mount Everest."
  )
];

final List<SmallItemData> holidayIslands = [
  const SmallItemData(
      name: "Bali",
      imagePath: "https://imageio.forbes.com/specials-images/imageserve/675172642/pura-ulun-danu-bratan-temple-in-Bali-/960x0.jpg?format=jpg&width=1440",
      city: "Denpasar",
      rating: "4.9",
      info: "Bali, Indoneziyaning mashhur oroli bo'lib, chiroyli plyajlari, boy madaniyati va mashhur sayyohlik maskanlari bilan mashhur. Bali oroli plyajda dam olish, suv sporti, madaniy va tarixiy joylarni o'rganish uchun ideal joy hisoblanadi."
  ),
  const SmallItemData(
      name: "Maldives",
      imagePath: "https://afar.brightspotcdn.com/dims4/default/150132a/2147483647/strip/true/crop/3000x1592+0+323/resize/1440x764!/format/webp/quality/90/?url=https%3A%2F%2Fk3-prod-afar-media.s3.us-west-2.amazonaws.com%2Fbrightspot%2Fb2%2Ff4%2F9a1ebe3f427f8e5eb937f8df8998%2Ftravelguides-maldives-videomediastudioeurope-shutterstock.jpg",
      city: "Malé",
      rating: "4.8",
      info: "Maldiv orollari, Hind okeanidagi tropik orollar guruhidir. Bu yerda siz oq qumli plyajlar, musaffo suvlar va dengiz osti dunyosining ajoyib manzaralarini topasiz. Maldivlar sayohatchilar uchun oromgoh va romantik sayohatlar uchun ideal joydir."
  ),
  const SmallItemData(
      name: "Hawaii",
      imagePath: "https://www.gohawaii.com/sites/default/files/styles/image_gallery_bg_xl/public/Hawaii%20Tourism%20Oceania%20%2817%29.jpg?itok=pRE80Dj_&timestamp=1583888246",
      city: "Honolulu",
      rating: "4.7",
      info: "Havayi orollari, Qo'shma Shtatlarning Tinch okeanidagi tropik orollari bo'lib, vulkanik landshaftlari, plyajlari va ko'plab tabiatni sevuvchilar uchun mashhur joydir. Bu orollarda siz surfing, snorkeling va sayohat qilishdan zavqlanishingiz mumkin."
  ),
  const SmallItemData(
      name: "Santorini",
      imagePath: "https://www.santoriniexperts.com/wp-content/uploads/sunset-santorini.jpg",
      city: "Thira",
      rating: "4.6",
      info: "Santorini, Gretsiyaning Egey dengizidagi mashhur oroli bo'lib, ajoyib ko'rinishlari, oq binolari va blyudlaridan zavqlanish uchun mashhurdir. Santorini oroli sayohatchilar uchun romantik oromgoh va tarixiy joylarni o'rganish uchun ideal."
  ),
  const SmallItemData(
      name: "Seychelles",
      imagePath: "https://www.telegraph.co.uk/content/dam/Travel/Destinations/Africa/Seychelles/AP78907278_Seychelles_Trave.jpg?imwidth=1280",
      city: "Victoria",
      rating: "4.8",
      info: "Seychelles orollari, Hind okeanida joylashgan tropik orollar guruhidir. Bu yerda siz oq qumli plyajlar, musaffo suvlar va tabiiy go'zalliklarni topasiz. Seychelles sayohatchilar uchun oromgoh va romantik sayohatlar uchun ideal joydir."
  ),
  const SmallItemData(
      name: "Phuket",
      imagePath: "https://www.letsphuket.com/wp-content/uploads/phuket1-1024x640.jpg",
      city: "Phuket City",
      rating: "4.5",
      info: "Phuket, Tailandning eng yirik oroli bo'lib, chiroyli plyajlari, ko'plab diqqatga sazovor joylari va faoliyatlari bilan mashhur. Bu yerda siz diving, snorkeling va plyajda dam olishdan zavqlanishingiz mumkin."
  ),
  const SmallItemData(
      name: "Bora Bora",
      imagePath: "https://www.tahititourisme.com/app/uploads/iris-images/15007/p2-08-bora-bora-divers-tnh-87-a-gregoire-le-bacon-tahiti-nui-helicopters-1-scaled-1600x1400-f50_50.jpg",
      city: "Vaitape",
      rating: "4.9",
      info: "Bora Bora, Fransiya Polineziyasidagi mashhur tropik oroli bo'lib, ajoyib plyajlari va turkiz rangli suvlar bilan mashhur. Bu orolda siz suzish, snorkeling va romantik dam olishni o'z ichiga olgan ko'plab faoliyatlardan zavqlanishingiz mumkin."
  ),
];

final Map<int, List<SmallItemData>> mapAll = {
  1: mountainList,
  2: holidayIslands,
  3: travelData
};


List<SmallItemData> getListFromMap(int key) {
  if (mapAll.containsKey(key)) {
    return mapAll[key]!;
  } else {
    // If the key is not found, combine all lists
    List<SmallItemData> combinedList = [];
    mapAll.forEach((_, list) {
      combinedList.addAll(list);
    });
    combinedList.shuffle();
    return combinedList;
  }
}

List<SmallItemData> searchByName(String name) {
  List<SmallItemData> resultList = [];

  mapAll.forEach((key, list) {
    list.forEach((item) {
      if (item.name.toLowerCase().startsWith(name.toLowerCase())) {
        resultList.add(item);
      }
    });
  });

  return resultList;
}

